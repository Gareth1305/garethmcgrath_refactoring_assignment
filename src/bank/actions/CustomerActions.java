package bank.actions;

import bank.dao.AccountTransaction;
import bank.dao.CustomerAccount;
import bank.dao.CustomerCurrentAccount;
import bank.menu_frames.Menu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;

/**
 * Created by Gareth1305 on 26/02/2016.
 * This class represents different methods in which a customer can call on their a/c
 */

public class CustomerActions {

    JFrame f;

    public void getCustomerStatement(CustomerAccount acc) {
        f = new JFrame("Customer Menu");
        f.setSize(400, 600);
        f.setLocation(200, 200);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        f.setVisible(true);

        JLabel label1 = new JLabel("Summary of account transactions: ");

        JPanel returnPanel = new JPanel();
        JButton returnButton = new JButton("Return");
        returnPanel.add(returnButton);

        JPanel textPanel = new JPanel();

        textPanel.setLayout(new BorderLayout());
        JTextArea textArea = new JTextArea(40, 20);
        textArea.setEditable(false);
        textPanel.add(label1, BorderLayout.NORTH);
        textPanel.add(textArea, BorderLayout.CENTER);
        textPanel.add(returnButton, BorderLayout.SOUTH);

        JScrollPane scrollPane = new JScrollPane(textArea);
        textPanel.add(scrollPane);

        for (int i = 0; i < acc.getTransactionList().size(); i++) {
            textArea.append(acc.getTransactionList().get(i).toString());
        }

        textPanel.add(textArea);

        Container content = f.getContentPane();
        content.setLayout(new GridLayout(1, 1));
        //content.add(label1);
        content.add(textPanel);
        //content.add(returnPanel);

        returnButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                f.dispose();
            }
        });
    }

    public void makeLodgement(CustomerAccount acc) {
        if (acc instanceof CustomerCurrentAccount) {
            if (requestPinForCurrentAccount(acc)) {
                lodgeAndUpdateBalance(acc);
            }
        } else {
            lodgeAndUpdateBalance(acc);
        }
    }

    private void lodgeAndUpdateBalance(CustomerAccount acc) {
        double balance = 0;
        String balanceTest = JOptionPane.showInputDialog(f, "Enter amount you wish to lodge:");
        if (Menu.isNumeric(balanceTest)) {
            balance = Double.parseDouble(balanceTest);
        } else {
            JOptionPane.showMessageDialog(f, "You must enter a numerical value!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
        }

        String euro = "\u20ac";
        acc.setBalance(acc.getBalance() + balance);
        Date date = new Date();
        String date2 = date.toString();
        String type = "Lodgement";
        double amount = balance;

        AccountTransaction transaction = new AccountTransaction(date2, type, amount);
        acc.getTransactionList().add(transaction);

        JOptionPane.showMessageDialog(f, balance + euro + " added do you account!", "Lodgement", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(f, "New balance = " + acc.getBalance() + euro, "Lodgement", JOptionPane.INFORMATION_MESSAGE);
    }

    // This method is called when the account type is current and the user
    // wished to make a transaction. The user's a/c validity is checked. If their
    // a/c is invalid then they cannot proceed with the transaction. If valid, they
    // are asked for their pin. If this is entered incorrectly 3 times then their
    // account becomes locked.
    public boolean requestPinForCurrentAccount(CustomerAccount acc) {
        int count = 3;
        if (((CustomerCurrentAccount) acc).getAtm().isValid()) {
            do {
                String Pin = JOptionPane.showInputDialog(f, "Enter 4 digit PIN;");
                int enteredPin = Integer.parseInt(Pin);
                int customersPin = ((CustomerCurrentAccount) acc).getAtm().getPin();
                if (enteredPin == customersPin) {
                    return true;
                } else {
                    count--;
                    if (count != 0) {
                        JOptionPane.showMessageDialog(f, "Incorrect pin. " + count + " attempts remaining.", "Pin", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                if (count == 0) {
                    JOptionPane.showMessageDialog(f, "Pin entered incorrectly 3 times. ATM card locked.", "Pin", JOptionPane.INFORMATION_MESSAGE);
                    ((CustomerCurrentAccount) acc).getAtm().setValid(false);
                }
            } while (count > 0);
        } else {
            JOptionPane.showMessageDialog(f, "Your Pin is locked. Try again later!", "Pin Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return false;
    }

    public void makeWithdraw(CustomerAccount acc) {
        if (acc instanceof CustomerCurrentAccount) {
            if (requestPinForCurrentAccount(acc)) {
                withdrawAndUpdateBalance(acc);
            }
        } else {
            withdrawAndUpdateBalance(acc);
        }
    }

    private void withdrawAndUpdateBalance(CustomerAccount acc) {
        double withdraw = 0;
        String balanceTest = JOptionPane.showInputDialog(f, "Enter amount you wish to withdraw (max 500):");
        if (Menu.isNumeric(balanceTest)) {
            withdraw = Double.parseDouble(balanceTest);
        } else {
            JOptionPane.showMessageDialog(f, "You must enter a numerical value!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
        }
        if (withdraw > 500) {
            JOptionPane.showMessageDialog(f, "500 is the maximum you can withdraw at a time.", "Oops!", JOptionPane.INFORMATION_MESSAGE);
            withdraw = 0;
        }
        if (withdraw > acc.getBalance()) {
            JOptionPane.showMessageDialog(f, "Insufficient funds.", "Oops!", JOptionPane.INFORMATION_MESSAGE);
            withdraw = 0;
        }

        String euro = "\u20ac";
        acc.setBalance(acc.getBalance() - withdraw);
        Date date = new Date();
        String date2 = date.toString();

        String type = "Withdraw";
        double amount = withdraw;

        AccountTransaction transaction = new AccountTransaction(date2, type, amount);
        acc.getTransactionList().add(transaction);

        JOptionPane.showMessageDialog(f, withdraw + euro + " withdrawn.", "Withdraw", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(f, "New balance = " + acc.getBalance() + euro, "Withdraw", JOptionPane.INFORMATION_MESSAGE);
    }

}