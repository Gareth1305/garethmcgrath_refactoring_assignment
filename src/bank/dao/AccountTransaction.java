package bank.dao;

public class AccountTransaction {
    private String date;
    private String type;
    private double amount;

    public AccountTransaction() {
    }

    public AccountTransaction(String date, String type, double amount) {
        this.date = date;
        this.type = type;
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String toString() {
        return "\n" + "Date = " + this.date + "\n"
                + "Type = " + this.type + "\n"
                + "Amount = " + this.amount + "\n";
    }

}
