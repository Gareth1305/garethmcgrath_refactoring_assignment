package bank_test;

import bank.actions.CustomerActions;
import bank.dao.*;
import bank.menu_frames.Menu;
import org.junit.Test;

import javax.swing.*;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Gareth1305 on 26/02/2016.
 * Testing all information regarding the user
 */

public class Customer_Tests {
    ArrayList<Customer> customers;

    public void createUsers() {
        customers = new ArrayList<>();
        Customer daenerys = new Customer("12345", "Targaryen", "Daenerys", "26-10-1986", "ID1", "motherOfDragons", null);
        Customer john = new Customer("54321", "Snow", "John", "26-12-1986", "Ghost", "ID2", null);
        Customer margaery = new Customer("678910", "Tyrell", "Margaery", "11-02-1982", "ID3", "aGameOfThrones", null);

        customers.add(daenerys);
        customers.add(john);
        customers.add(margaery);
    }

    // Test below check to see if the array size is 3 after 3 customers are added
    @Test
    public void addCustToArrayList() {
        createUsers();
        assertEquals(3, customers.size());
        assertEquals(null, customers.get(0).getAccounts());
    }

    // Test below checks my getCustWithID() method. This returns true if a users id matches
    // one of the ids of the users that we just created
    @Test
    public void testFindCustById() {
        if (customers == null) {
            createUsers();
        }
        JOptionPane.showMessageDialog(null,
                "This test looks for a customer with a \n" +
                        "specific id. A valid ID to search is (ID1)",
                "Test Information!",
                JOptionPane.WARNING_MESSAGE);
        Customer cust = Menu.getCustWithID(customers);
        assertNotNull(cust);
    }

    // Test below check my requestPinForCurrentAccount() method. The user is asked to provide
    // the wrong pin 3 times. If this is done. A check is performed to make sure that the
    // account has been set to invalid.
    @Test
    public void lockPin() {
        ATMCard atmCard = new ATMCard(1234, true);
        CustomerCurrentAccount currentAccount = new CustomerCurrentAccount(atmCard, "12345", 0, null);
        ArrayList<CustomerAccount> accounts = new ArrayList<>();
        accounts.add(currentAccount);
        Customer customer = new Customer("12345", "Targaryen", "Daenerys", "26-10-1986", "ID1", "motherOfDragons", accounts);
        JOptionPane.showMessageDialog(null,
                "This test will make sure the atm card is invalid after 3 \n incorrect pin attempts are entered. Do not enter 1234",
                "Test Information!",
                JOptionPane.WARNING_MESSAGE);
        CustomerActions c = new CustomerActions();
        c.requestPinForCurrentAccount(customer.getAccounts().get(0));
        assertEquals(false, currentAccount.getAtm().isValid());
    }

    // Test below ensures that my makeLodgement() method works correctly. The user will
    // add 50.0 to the set balance of 100.0 and a check will be done to see if the new
    // balance is equal to 150.0
    @Test
    public void testLodgements() {
        AccountTransaction accountTransaction = new AccountTransaction();
        ArrayList<AccountTransaction> transactions = new ArrayList<>();
        transactions.add(accountTransaction);
        CustomerDepositAccount currentAccount = new CustomerDepositAccount(1.56, "12345", 0.00, transactions);
        ArrayList<CustomerAccount> accounts = new ArrayList<>();
        accounts.add(currentAccount);
        Customer customer = new Customer("12345", "Targaryen", "Daenerys", "26-10-1986", "ID1", "motherOfDragons", accounts);
        customer.getAccounts().get(0).setBalance(100.00);
        assertEquals(100.00, customer.getAccounts().get(0).getBalance(), 1e-15);
        JOptionPane.showMessageDialog(null,
                "This test checks the balance after 50.00 is entered. Please enter 50.00!",
                "Test Information!",
                JOptionPane.WARNING_MESSAGE);
        CustomerActions c = new CustomerActions();
        c.makeLodgement(customer.getAccounts().get(0));
        //Add 50 to your account
        assertEquals(150.00, customer.getAccounts().get(0).getBalance(), 1e-15);

    }

    // Test below ensures that my makeWithdraw() method works correctly. The user will
    // take 50.0 away from the set balance of 100.0 and a check will be done to see if
    // the new balance is equal to 50.0
    @Test
    public void testWithdrawl() {
        AccountTransaction accountTransaction = new AccountTransaction();
        ArrayList<AccountTransaction> transactions = new ArrayList<>();
        transactions.add(accountTransaction);
        CustomerDepositAccount currentAccount = new CustomerDepositAccount(1.56, "12345", 0.00, transactions);
        ArrayList<CustomerAccount> accounts = new ArrayList<>();
        accounts.add(currentAccount);
        Customer customer = new Customer("12345", "Targaryen", "Daenerys", "26-10-1986", "ID1", "motherOfDragons", accounts);
        customer.getAccounts().get(0).setBalance(100.00);
        assertEquals(100.00, customer.getAccounts().get(0).getBalance(), 1e-15);
        JOptionPane.showMessageDialog(null,
                "This test checks the balance after 50.00 is withdrawn. Please enter 50.00!",
                "Test Information!",
                JOptionPane.WARNING_MESSAGE);
        CustomerActions c = new CustomerActions();
        c.makeWithdraw(customer.getAccounts().get(0));
        //Add 50 to your account
        assertEquals(50.00, customer.getAccounts().get(0).getBalance(), 1e-15);
    }
}
