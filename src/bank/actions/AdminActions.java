package bank.actions;

import bank.dao.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import bank.menu_frames.CreateJFrame;
import bank.menu_frames.Menu;

/**
 * Created by Gareth1305 on 26/02/2016.
 * This class represents different options which an admin can do.
 */

public class AdminActions {
    JFrame f;
    int position = 0;
    JLabel firstNameLabel, surnameLabel, pPPSLabel, dOBLabel, customerIDLabel, passwordLabel;
    JTextField firstNameTextField, surnameTextField, pPSTextField, dOBTextField;
    JTextField customerIDTextField, passwordTextField;

    public void addAccountToCust(ArrayList<Customer> customerList) {
        if (customerList.isEmpty() || customerList.size() == 0) {
            JOptionPane.showMessageDialog(null, "There are currently no customers to display. ");
        } else {
            Customer custToAddTo = Menu.getCustWithID(customerList);
            if (custToAddTo == null) {
                int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    addAccountToCust(customerList);
                }
            } else {
                String[] choices = {"Current Account", "Deposit Account"};
                String account = (String) JOptionPane.showInputDialog(null, "Please choose account type",
                        "Account Type", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
                double balance = 0;
                if (account.equals("Current Account")) {
                    String number = String.valueOf("C" + (customerList.indexOf(custToAddTo) + 1) * 10 + (custToAddTo.getAccounts().size() + 1));
                    ArrayList<AccountTransaction> transactionList = new ArrayList<>();
                    int randomPIN = (int) (Math.random() * 9000) + 1000;

                    ATMCard atm = new ATMCard(randomPIN, true);
                    CustomerCurrentAccount current = new CustomerCurrentAccount(atm, number, balance, transactionList);

                    custToAddTo.getAccounts().add(current);
                    JOptionPane.showMessageDialog(f, "Account number = " + number + "\n PIN = " + String.valueOf(randomPIN), "Account created.", JOptionPane.INFORMATION_MESSAGE);

                } else if (account.equals("Deposit Account")) {
                    double interest = 0;
                    String number = String.valueOf("D" + (customerList.indexOf(custToAddTo) + 1) * 10 + (custToAddTo.getAccounts().size() + 1));
                    ArrayList<AccountTransaction> transactionList = new ArrayList<>();

                    CustomerDepositAccount deposit = new CustomerDepositAccount(interest, number, balance, transactionList);

                    custToAddTo.getAccounts().add(deposit);
                    JOptionPane.showMessageDialog(f, "Account number = " + number, "Account created.", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }

    public void editExistingCustomer(ArrayList<Customer> customers) {
        if (customers.isEmpty() || customers.size() == 0) {
            JOptionPane.showMessageDialog(null, "There are currently no customers to display. ");
        } else {
            Customer custToEdit = Menu.getCustWithID(customers);
            if (custToEdit == null) {
                int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    editExistingCustomer(customers);
                }
            } else {
                f = CreateJFrame.createJFrame("Edit Current Customer", 400, 300, 200, 200);

                firstNameLabel = new JLabel("First Name:", SwingConstants.LEFT);
                surnameLabel = new JLabel("Surname:", SwingConstants.LEFT);
                pPPSLabel = new JLabel("PPS Number:", SwingConstants.LEFT);
                dOBLabel = new JLabel("Date of birth", SwingConstants.LEFT);
                customerIDLabel = new JLabel("CustomerID:", SwingConstants.LEFT);
                passwordLabel = new JLabel("Password:", SwingConstants.LEFT);
                firstNameTextField = new JTextField(20);
                surnameTextField = new JTextField(20);
                pPSTextField = new JTextField(20);
                dOBTextField = new JTextField(20);
                customerIDTextField = new JTextField(20);
                passwordTextField = new JTextField(20);

                JPanel textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                JPanel cancelPanel = new JPanel();

                textPanel.add(firstNameLabel);
                textPanel.add(firstNameTextField);
                textPanel.add(surnameLabel);
                textPanel.add(surnameTextField);
                textPanel.add(pPPSLabel);
                textPanel.add(pPSTextField);
                textPanel.add(dOBLabel);
                textPanel.add(dOBTextField);
                textPanel.add(customerIDLabel);
                textPanel.add(customerIDTextField);
                textPanel.add(passwordLabel);
                textPanel.add(passwordTextField);

                if (custToEdit != null) {
                    firstNameTextField.setText(custToEdit.getFirstName());
                    surnameTextField.setText(custToEdit.getSurname());
                    pPSTextField.setText(custToEdit.getPPS());
                    dOBTextField.setText(custToEdit.getDOB());
                    customerIDTextField.setText(custToEdit.getCustomerID());
                    passwordTextField.setText(custToEdit.getPassword());
                }

                JButton saveButton = new JButton("Save");
                JButton cancelButton = new JButton("Exit");

                cancelPanel.add(cancelButton, BorderLayout.SOUTH);
                cancelPanel.add(saveButton, BorderLayout.SOUTH);
                Container content = f.getContentPane();
                content.setLayout(new GridLayout(2, 1));
                content.add(textPanel, BorderLayout.NORTH);
                content.add(cancelPanel, BorderLayout.SOUTH);

                f.setContentPane(content);
                f.setSize(340, 350);
                f.setLocation(200, 200);
                f.setVisible(true);
                f.setResizable(false);

                final Customer finalCustomer = custToEdit;
                saveButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        if (finalCustomer != null) {
                            finalCustomer.setFirstName(firstNameTextField.getText());
                            finalCustomer.setSurname(surnameTextField.getText());
                            finalCustomer.setPPS(pPSTextField.getText());
                            finalCustomer.setDOB(dOBTextField.getText());
                            finalCustomer.setCustomerID(customerIDTextField.getText());
                            finalCustomer.setPassword(passwordTextField.getText());

                            JOptionPane.showMessageDialog(null, "Changes Saved.");
                        }
                    }
                });

                cancelButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        f.dispose();
                    }
                });
            }
        }
    }

    public void navigateCustCollection(final ArrayList<Customer> customers) {
        f = new JFrame("Navigation Menu");
        f.setSize(400, 300);
        f.setLocation(200, 200);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        f.setVisible(true);
        if (customers.isEmpty()) {
            JOptionPane.showMessageDialog(null, "There are currently no customers to display. ");
        } else {
            JButton first, previous, next, last, cancel;
            JPanel gridPanel, buttonPanel, cancelPanel;

            Container content = f.getContentPane();
            content.setLayout(new BorderLayout());

            buttonPanel = new JPanel();
            gridPanel = new JPanel(new GridLayout(8, 2));
            cancelPanel = new JPanel();

            firstNameLabel = new JLabel("First Name:", SwingConstants.LEFT);
            surnameLabel = new JLabel("Surname:", SwingConstants.LEFT);
            pPPSLabel = new JLabel("PPS Number:", SwingConstants.LEFT);
            dOBLabel = new JLabel("Date of birth", SwingConstants.LEFT);
            customerIDLabel = new JLabel("CustomerID:", SwingConstants.LEFT);
            passwordLabel = new JLabel("Password:", SwingConstants.LEFT);
            firstNameTextField = new JTextField(20);
            surnameTextField = new JTextField(20);
            pPSTextField = new JTextField(20);
            dOBTextField = new JTextField(20);
            customerIDTextField = new JTextField(20);
            passwordTextField = new JTextField(20);

            first = new JButton("First");
            previous = new JButton("Previous");
            next = new JButton("Next");
            last = new JButton("Last");
            cancel = new JButton("Cancel");

            setCustomerInformation(position, customers);

            firstNameTextField.setEditable(false);
            surnameTextField.setEditable(false);
            pPSTextField.setEditable(false);
            dOBTextField.setEditable(false);
            customerIDTextField.setEditable(false);
            passwordTextField.setEditable(false);

            gridPanel.add(firstNameLabel);
            gridPanel.add(firstNameTextField);
            gridPanel.add(surnameLabel);
            gridPanel.add(surnameTextField);
            gridPanel.add(pPPSLabel);
            gridPanel.add(pPSTextField);
            gridPanel.add(dOBLabel);
            gridPanel.add(dOBTextField);
            gridPanel.add(customerIDLabel);
            gridPanel.add(customerIDTextField);
            gridPanel.add(passwordLabel);
            gridPanel.add(passwordTextField);

            buttonPanel.add(first);
            buttonPanel.add(previous);
            buttonPanel.add(next);
            buttonPanel.add(last);

            cancelPanel.add(cancel);

            content.add(gridPanel, BorderLayout.NORTH);
            content.add(buttonPanel, BorderLayout.CENTER);
            content.add(cancelPanel, BorderLayout.AFTER_LAST_LINE);
            first.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    position = 0;
                    setCustomerInformation(position, customers);
                }
            });

            previous.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    if (position >= 1) {
                        position = position - 1;
                        setCustomerInformation(position, customers);
                    }
                }
            });

            next.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    if (position != (customers.size() - 1)) {
                        position = position + 1;
                        setCustomerInformation(position, customers);
                    }
                }
            });

            last.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    position = customers.size() - 1;
                    setCustomerInformation(position, customers);
                }
            });

            cancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    f.dispose();
                }
            });
            f.setContentPane(content);
            f.setSize(400, 300);
            f.setVisible(true);
        }
    }

    public void displaySummaryOfAccounts(ArrayList<Customer> customers, CustomerAccount acc) {
        f = new JFrame("Summary of Transactions");
        f.setSize(400, 700);
        f.setLocation(200, 200);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        f.setVisible(true);

        JLabel label1 = new JLabel("Summary of all transactions: ");

        JPanel returnPanel = new JPanel();
        JButton returnButton = new JButton("Return");
        returnPanel.add(returnButton);

        JPanel textPanel = new JPanel();

        textPanel.setLayout(new BorderLayout());
        JTextArea textArea = new JTextArea(40, 20);
        textArea.setEditable(false);
        textPanel.add(label1, BorderLayout.NORTH);
        textPanel.add(textArea, BorderLayout.CENTER);
        textPanel.add(returnButton, BorderLayout.SOUTH);

        JScrollPane scrollPane = new JScrollPane(textArea);
        textPanel.add(scrollPane);

        for (Customer aCustomerList : customers) {
            for (int b = 0; b < aCustomerList.getAccounts().size(); b++) {
                acc = aCustomerList.getAccounts().get(b);
                for (int c = 0; c < aCustomerList.getAccounts().get(b).getTransactionList().size(); c++) {
                    textArea.append(acc.getTransactionList().get(c).toString());
                }
            }
        }

        textPanel.add(textArea);

        Container content = f.getContentPane();
        content.setLayout(new GridLayout(1, 1));
        //	content.add(label1);
        content.add(textPanel);
        //content.add(returnPanel);

        returnButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                f.dispose();
            }
        });
    }

    public void deleteExistingCustomer(ArrayList<Customer> customers) {
        if (customers.isEmpty() || customers.size() == 0) {
            JOptionPane.showMessageDialog(null, "There are currently no customers to display. ");
        } else {
            Customer custToDelete = Menu.getCustWithID(customers);
            if (custToDelete == null) {
                int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    deleteExistingCustomer(customers);
                }
            } else {
                // If the customer has accounts the user must delete these first. If they do
                // not have accounts then the user gets deleted
                if (custToDelete.getAccounts().size() > 0) {
                    JOptionPane.showMessageDialog(f, "This customer has accounts. \n You " +
                                    "must delete a customer's accounts before deleting a customer ",
                            "Oops!", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    customers.remove(custToDelete);
                    JOptionPane.showMessageDialog(f, "Customer Deleted ", "Success.", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }

    // This method was created to avoid code duplication in the navigateCustCollection method
    private void setCustomerInformation(int position, ArrayList<Customer> customers) {
        firstNameTextField.setText(customers.get(position).getFirstName());
        surnameTextField.setText(customers.get(position).getSurname());
        pPSTextField.setText(customers.get(position).getPPS());
        dOBTextField.setText(customers.get(position).getDOB());
        customerIDTextField.setText(customers.get(position).getCustomerID());
        passwordTextField.setText(customers.get(position).getPassword());
    }
}
