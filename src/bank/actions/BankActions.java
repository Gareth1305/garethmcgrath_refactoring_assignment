package bank.actions;

import bank.dao.Customer;
import bank.dao.CustomerAccount;
import bank.dao.CustomerCurrentAccount;
import bank.dao.CustomerDepositAccount;
import bank.menu_frames.Menu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * Created by Gareth1305 on 26/02/2016.
 * This class represents different methods in relation to a customers bank account.
 * These methods are called by the admin and either apply interest or apply charges to the customers a/c
 */

public class BankActions extends JFrame {
    JFrame f;

    private CustomerAccount acc = new CustomerAccount();

    public void applyBankCharges(ArrayList<Customer> customers) {
        if (customers.isEmpty()) {
            JOptionPane.showMessageDialog(f, "There are no customers yet!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
        } else {
            Customer custToAddChargesTo = Menu.getCustWithID(customers);
            if (custToAddChargesTo == null) {
                int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    applyBankCharges(customers);
                }
            } else {
                if (custToAddChargesTo.getAccounts().size() == 0) {
                    JOptionPane.showMessageDialog(f, "This customer has no accounts. \n You " +
                                    "cannot add interest to a customer with no accounts ",
                            "Oops!", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    f = new JFrame("Administrator Menu");
                    f.setSize(400, 300);
                    f.setLocation(200, 200);
                    f.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent we) {
                            System.exit(0);
                        }
                    });
                    f.setVisible(true);

                    JComboBox<String> box = new JComboBox<>();
                    for (int i = 0; i < custToAddChargesTo.getAccounts().size(); i++) {
                        box.addItem(custToAddChargesTo.getAccounts().get(i).getNumber());
                    }

                    box.getSelectedItem();

                    JPanel boxPanel = new JPanel();
                    boxPanel.add(box);

                    JPanel buttonPanel = new JPanel();
                    JButton continueButton = new JButton("Apply Charge");
                    JButton returnButton = new JButton("Return");
                    buttonPanel.add(continueButton);
                    buttonPanel.add(returnButton);
                    Container content = f.getContentPane();
                    content.setLayout(new GridLayout(2, 1));

                    content.add(boxPanel);
                    content.add(buttonPanel);

                    for (int i = 0; i < custToAddChargesTo.getAccounts().size(); i++) {
                        if (custToAddChargesTo.getAccounts().get(i).getNumber() == box.getSelectedItem()) {
                            acc = custToAddChargesTo.getAccounts().get(i);
                        }
                    }

                    continueButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            String euro = "\u20ac";

                            if (acc instanceof CustomerDepositAccount) {
                                JOptionPane.showMessageDialog(f, "25" + euro + " deposit account fee aplied.", "", JOptionPane.INFORMATION_MESSAGE);
                                acc.setBalance(acc.getBalance() - 25);
                                JOptionPane.showMessageDialog(f, "New balance = " + acc.getBalance(), "Success!", JOptionPane.INFORMATION_MESSAGE);
                            }

                            if (acc instanceof CustomerCurrentAccount) {
                                JOptionPane.showMessageDialog(f, "15" + euro + " current account fee aplied.", "", JOptionPane.INFORMATION_MESSAGE);
                                acc.setBalance(acc.getBalance() - 25);
                                JOptionPane.showMessageDialog(f, "New balance = " + acc.getBalance(), "Success!", JOptionPane.INFORMATION_MESSAGE);
                            }
                            f.dispose();
                        }
                    });

                    returnButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            f.dispose();
                        }
                    });
                }
            }
        }
    }

    public void applyInterest(ArrayList<Customer> customers) {
        if (customers.isEmpty()) {
            JOptionPane.showMessageDialog(f, "There are no customers yet!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
        } else {
            Customer custToAddInterestTo = Menu.getCustWithID(customers);
            if (custToAddInterestTo == null) {
                int reply = JOptionPane.showConfirmDialog(null, null, "User not found. Try again?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    applyInterest(customers);
                }
            } else {
                if (custToAddInterestTo.getAccounts().size() == 0) {
                    JOptionPane.showMessageDialog(f, "This customer has no accounts. \n You " +
                                    "cannot add interest to a customer with no accounts ",
                            "Oops!", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    f = new JFrame("Administrator Menu");
                    f.setSize(400, 300);
                    f.setLocation(200, 200);
                    f.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent we) {
                            System.exit(0);
                        }
                    });
                    f.setVisible(true);

                    JComboBox<String> box = new JComboBox<>();
                    for (int i = 0; i < custToAddInterestTo.getAccounts().size(); i++) {
                        box.addItem(custToAddInterestTo.getAccounts().get(i).getNumber());
                    }

                    box.getSelectedItem();

                    JPanel boxPanel = new JPanel();
                    JLabel label = new JLabel("Select an account to apply interest to:");
                    boxPanel.add(label);
                    boxPanel.add(box);
                    JPanel buttonPanel = new JPanel();
                    JButton continueButton = new JButton("Apply Interest");
                    JButton returnButton = new JButton("Return");
                    buttonPanel.add(continueButton);
                    buttonPanel.add(returnButton);
                    Container content = f.getContentPane();
                    content.setLayout(new GridLayout(2, 1));

                    content.add(boxPanel);
                    content.add(buttonPanel);

                    for (int i = 0; i < custToAddInterestTo.getAccounts().size(); i++) {
                        if (custToAddInterestTo.getAccounts().get(i).getNumber() == box.getSelectedItem()) {
                            acc = custToAddInterestTo.getAccounts().get(i);
                        }
                    }

                    continueButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            String euro = "\u20ac";
                            boolean loop = true;

                            while (loop) {
                                String interestString = JOptionPane.showInputDialog(f, "Enter interest percentage you wish to apply: \n NOTE: Please enter a numerical value. (with no percentage sign) \n E.g: If you wish to apply 8% interest, enter '8'");//the isNumeric method tests to see if the string entered was numeric.
                                if (Menu.isNumeric(interestString)) {
                                    double interest = Double.parseDouble(interestString);
                                    loop = false;
                                    acc.setBalance(acc.getBalance() + (acc.getBalance() * (interest / 100)));
                                    JOptionPane.showMessageDialog(f, interest + "% interest applied. \n new balance = " + acc.getBalance() + euro, "Success!", JOptionPane.INFORMATION_MESSAGE);
                                } else {
                                    JOptionPane.showMessageDialog(f, "You must enter a numerical value!", "Oops!", JOptionPane.INFORMATION_MESSAGE);
                                }
                            }
                        }
                    });

                    returnButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            f.dispose();
                        }
                    });
                }
            }
        }
    }
}
