package bank.menu_frames;

import bank.actions.AdminActions;
import bank.actions.BankActions;
import bank.actions.CustomerActions;
import bank.dao.Customer;
import bank.dao.CustomerAccount;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

/**
 * I have seperated the funtionality into numerous different methods from what it
 * originally was. I now have a seperate method for each actionlistener that was on the
 * main menu. I have also seperated functionality into seperate classes depending on the
 * users selection type. For example, I have created a class called CustomerAction.java
 * which will allow the user to lodge & withdraw from their a/c as well as display thier statement.
 * This is similar for the AdminActions.java and BankActions.java classes
 */

public class Menu extends JFrame {

    private ArrayList<Customer> customerList;
    private CustomerAccount acc = new CustomerAccount();
    JFrame frame, frame2;
    JLabel firstNameLabel, surnameLabel, pPPSLabel, dOBLabel;
    JTextField firstNameTextField, surnameTextField, pPSTextField, dOBTextField;
    Container content;
    JPanel panel2;

    public ArrayList<Customer> getCustomerList() {
        if (customerList == null) {
            customerList = new ArrayList<>();
            return customerList;
        } else {
            return customerList;
        }
    }

    public void menuStart() {
        frame = CreateJFrame.createJFrame("User Type", 400, 300, 200, 200);

        JPanel userTypePanel = new JPanel();
        final ButtonGroup userType = new ButtonGroup();
        JRadioButton radioButton;

        userTypePanel.add(radioButton = new JRadioButton("Existing Customer"));
        radioButton.setActionCommand("Customer");
        userType.add(radioButton);

        userTypePanel.add(radioButton = new JRadioButton("Administrator"));
        radioButton.setActionCommand("Administrator");
        userType.add(radioButton);

        userTypePanel.add(radioButton = new JRadioButton("New Customer"));
        radioButton.setActionCommand("New Customer");
        userType.add(radioButton);

        JPanel continuePanel = new JPanel();
        JButton continueButton = new JButton("Continue");
        continuePanel.add(continueButton);

        Container content = frame.getContentPane();
        content.setLayout(new GridLayout(2, 1));
        content.add(userTypePanel);
        content.add(continuePanel);

        continueButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                String user = userType.getSelection().getActionCommand();
                switch (user) {
                    case "New Customer":
                        continueClickedOnNewCustomer();
                        break;
                    case "Administrator":
                        continueClickedOnAdministrator();
                        break;
                    case "Customer":
                        continueClickedOnExistingCustomer();
                        break;
                }
            }
        });
        frame.setVisible(true);
    }

    private void continueClickedOnExistingCustomer() {
        JTextField username = new JTextField();
        JTextField password = new JPasswordField();
        Object[] message = {
                "ID:", username,
                "Password:", password
        };
        int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            boolean found = false;
            for (Customer customer : customerList) {
                if (customer.getCustomerID().equals(username.getText()) && customer.getPassword().equals(password.getText())) {
                    found = true;
                    customer(customer);
                    break;
                }
            }
            if (!found) {
                int reply = JOptionPane.showConfirmDialog(null, "Incorrect ID or Password. Try again?", "Login Error!", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    continueClickedOnExistingCustomer();
                } else if (reply == JOptionPane.NO_OPTION) {
                    menuStart();
                }
            }
        } else {
            menuStart();
        }
    }

    private void continueClickedOnAdministrator() {
        JTextField username = new JTextField();
        JTextField password = new JPasswordField();
        Object[] message = {
                "Username:", username,
                "Password:", password
        };
        int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            if (username.getText().equals("admin") && password.getText().equals("admin11")) {
                frame.dispose();
                admin();
            } else {
                int reply = JOptionPane.showConfirmDialog(null, "Incorrect Username. Try again?", "Login Error!", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    continueClickedOnAdministrator();
                } else if (reply == JOptionPane.NO_OPTION) {
                    frame2.dispose();
                    menuStart();
                }
            }
        } else {
            menuStart();
        }
    }

    private void continueClickedOnNewCustomer() {
        JButton add, cancel;
        frame.dispose();
        frame2 = CreateJFrame.createJFrame("Create New Customer", 400, 300, 200, 200);
        Container content = frame2.getContentPane();
        content.setLayout(new BorderLayout());

        firstNameLabel = new JLabel("First Name:", SwingConstants.RIGHT);
        surnameLabel = new JLabel("Surname:", SwingConstants.RIGHT);
        pPPSLabel = new JLabel("PPS Number:", SwingConstants.RIGHT);
        dOBLabel = new JLabel("Date of birth", SwingConstants.RIGHT);
        firstNameTextField = new JTextField(20);
        surnameTextField = new JTextField(20);
        pPSTextField = new JTextField(20);
        dOBTextField = new JTextField(20);

        JPanel panel = new JPanel(new GridLayout(6, 2));
        panel.add(firstNameLabel);
        panel.add(firstNameTextField);
        panel.add(surnameLabel);
        panel.add(surnameTextField);
        panel.add(pPPSLabel);
        panel.add(pPSTextField);
        panel.add(dOBLabel);
        panel.add(dOBTextField);

        panel2 = new JPanel();
        add = new JButton("Add");
        cancel = new JButton("Cancel");

        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String PPS = pPSTextField.getText();
                String firstName = firstNameTextField.getText();
                String surname = surnameTextField.getText();
                String DOB = dOBTextField.getText();
                String CustomerID = "ID" + PPS;

                if (firstName.equals("") || PPS.equals("") || surname.equals("") || DOB.equals("")) {
                    JOptionPane.showMessageDialog(null, "Fields cannot be empty!", "Error!", JOptionPane.ERROR_MESSAGE);
                } else {
                    String password;
                    boolean isPassValid = false;
                    do {
                        password = JOptionPane.showInputDialog(frame, "Enter 7 character Password;");
                        if (password.length() != 7) {
                            JOptionPane.showMessageDialog(null, "Password Error!", "Error!", JOptionPane.ERROR_MESSAGE);
                        } else {
                            isPassValid = true;
                            ArrayList<CustomerAccount> accounts = new ArrayList<>();
                            Customer customer = new Customer(PPS, surname, firstName, DOB, CustomerID, password, accounts);
                            getCustomerList().add(customer);
                            JOptionPane.showMessageDialog(frame, "CustomerID = " + CustomerID + "\n Password = " + password, "Customer created.", JOptionPane.INFORMATION_MESSAGE);
                            frame2.dispose();
                            frame.dispose();
                            menuStart();
                        }
                    } while (!isPassValid);
                }
            }
        });

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame2.dispose();
                menuStart();
            }
        });

        panel2.add(add);
        panel2.add(cancel);

        content.add(panel, BorderLayout.CENTER);
        content.add(panel2, BorderLayout.SOUTH);

        frame2.setVisible(true);
    }

    public void admin() {
        dispose();
        frame = CreateJFrame.createJFrame("Administrator Menu", 400, 400, 200, 200);

        JPanel deleteCustomerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton deleteCustomer = new JButton("Delete Customer");
        deleteCustomer.setPreferredSize(new Dimension(250, 20));
        deleteCustomerPanel.add(deleteCustomer);

        JPanel deleteAccountPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton deleteAccount = new JButton("Delete Account");
        deleteAccount.setPreferredSize(new Dimension(250, 20));
        deleteAccountPanel.add(deleteAccount);

        JPanel bankChargesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton bankChargesButton = new JButton("Apply Bank Charges");
        bankChargesButton.setPreferredSize(new Dimension(250, 20));
        bankChargesPanel.add(bankChargesButton);

        JPanel interestPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton interestButton = new JButton("Apply Interest");
        interestPanel.add(interestButton);
        interestButton.setPreferredSize(new Dimension(250, 20));

        JPanel editCustomerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton editCustomerButton = new JButton("Edit existing Customer");
        editCustomerPanel.add(editCustomerButton);
        editCustomerButton.setPreferredSize(new Dimension(250, 20));

        JPanel navigatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton navigateButton = new JButton("Navigate Customer Collection");
        navigatePanel.add(navigateButton);
        navigateButton.setPreferredSize(new Dimension(250, 20));

        JPanel summaryPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton summaryButton = new JButton("Display Summary Of All Accounts");
        summaryPanel.add(summaryButton);
        summaryButton.setPreferredSize(new Dimension(250, 20));

        JPanel accountPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton accountButton = new JButton("Add an Account to a Customer");
        accountPanel.add(accountButton);
        accountButton.setPreferredSize(new Dimension(250, 20));

        JPanel returnPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton returnButton = new JButton("Exit Admin Menu");
        returnPanel.add(returnButton);

        JLabel label1 = new JLabel("Please select an option");

        content = frame.getContentPane();
        content.setLayout(new GridLayout(9, 1));
        content.add(label1);
        content.add(accountPanel);
        content.add(bankChargesPanel);
        content.add(interestPanel);
        content.add(editCustomerPanel);
        content.add(navigatePanel);
        content.add(summaryPanel);
        content.add(deleteCustomerPanel);
        content.add(returnPanel);

        bankChargesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                BankActions b = new BankActions();
                b.applyBankCharges(getCustomerList());
            }
        });

        interestButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                BankActions b = new BankActions();
                b.applyInterest(getCustomerList());
            }
        });

        editCustomerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                AdminActions a = new AdminActions();
                a.editExistingCustomer(getCustomerList());
            }
        });

        summaryButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                AdminActions a = new AdminActions();
                a.displaySummaryOfAccounts(getCustomerList(), acc);
            }
        });

        navigateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                AdminActions a = new AdminActions();
                a.navigateCustCollection(getCustomerList());
            }
        });

        accountButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                AdminActions a = new AdminActions();
                a.addAccountToCust(getCustomerList());
            }
        });

        deleteCustomer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                AdminActions a = new AdminActions();
                a.deleteExistingCustomer(getCustomerList());
            }
        });

        returnButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                frame.dispose();
                menuStart();
            }
        });
    }

    public void customer(final Customer e1) {
        frame = CreateJFrame.createJFrame("Customer Menu", 400, 300, 200, 200);

        if (e1.getAccounts() == null || e1.getAccounts().size() == 0) {
            JOptionPane.showMessageDialog(frame, "This customer does not have any accounts yet. \n An admin must create an account for this customer \n for them to be able to use customer functionality. ", "Oops!", JOptionPane.INFORMATION_MESSAGE);
            frame.dispose();
            menuStart();
        } else {
            JPanel buttonPanel = new JPanel();
            JPanel boxPanel = new JPanel();
            JPanel labelPanel = new JPanel();

            JLabel label = new JLabel("Select Account:");
            labelPanel.add(label);

            JButton returnButton = new JButton("Return");
            buttonPanel.add(returnButton);
            JButton continueButton = new JButton("Continue");
            buttonPanel.add(continueButton);

            final JComboBox<String> box = new JComboBox<>();
            for (int i = 0; i < e1.getAccounts().size(); i++) {
                box.addItem(e1.getAccounts().get(i).getNumber());
            }

            boxPanel.add(box);
            content = frame.getContentPane();
            content.setLayout(new GridLayout(3, 1));
            content.add(labelPanel);
            content.add(boxPanel);
            content.add(buttonPanel);

            returnButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    frame.dispose();
                    menuStart();
                }
            });

            continueButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    for (int i = 0; i < e1.getAccounts().size(); i++) {
                        if (e1.getAccounts().get(i).getNumber() == box.getSelectedItem()) {
                            acc = e1.getAccounts().get(i);
                        }
                    }
                    frame.dispose();
                    frame = CreateJFrame.createJFrame("Customer Menu", 400, 300, 200, 200);

                    JPanel statementPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                    JButton statementButton = new JButton("Display Bank Statement");
                    statementButton.setPreferredSize(new Dimension(250, 20));

                    statementPanel.add(statementButton);

                    JPanel lodgementPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                    JButton lodgementButton = new JButton("Lodge money into account");
                    lodgementPanel.add(lodgementButton);
                    lodgementButton.setPreferredSize(new Dimension(250, 20));

                    JPanel withdrawalPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                    JButton withdrawButton = new JButton("Withdraw money from account");
                    withdrawalPanel.add(withdrawButton);
                    withdrawButton.setPreferredSize(new Dimension(250, 20));

                    JPanel returnPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                    JButton returnButton = new JButton("Exit Customer Menu");
                    returnPanel.add(returnButton);

                    JLabel label1 = new JLabel("Please select an option");

                    content = frame.getContentPane();
                    content.setLayout(new GridLayout(5, 1));
                    content.add(label1);
                    content.add(statementPanel);
                    content.add(lodgementPanel);
                    content.add(withdrawalPanel);
                    content.add(returnPanel);

                    statementButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            CustomerActions c = new CustomerActions();
                            c.getCustomerStatement(acc);
                        }
                    });

                    lodgementButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            CustomerActions c = new CustomerActions();
                            c.makeLodgement(acc);
                        }
                    });

                    withdrawButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            CustomerActions c = new CustomerActions();
                            c.makeWithdraw(acc);
                        }
                    });

                    returnButton.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            frame.dispose();
                            menuStart();
                        }
                    });
                }
            });
        }
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    // A custom method which accepts an array of customers and ask the user to
    // enter the id of the customer that they are looking for. If the id is found,
    // the customer is returned to the user, if not, null is returned and a check
    // for null is performed in the method that originally called this method.
    public static Customer getCustWithID(ArrayList<Customer> customers) {
        Object customerID = JOptionPane.showInputDialog(null, "Customer ID of Customer:");
        for (Customer c : customers) {
            if (c.getCustomerID().equals(customerID)) {
                return c;
            }
        }
        return null;
    }
}