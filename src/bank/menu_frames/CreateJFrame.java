package bank.menu_frames;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Gareth1305 on 26/02/2016.
 * I have created this class to simply take in values and create a JFrame.
 * I felt this was needed as this same/similar code has been numerous times throughout this project.
 */

public class CreateJFrame extends JFrame {
    public static JFrame createJFrame(String title, int setSizeX, int setSizeY,
                                      int setLocX, int setLocY) {
        JFrame frame = new JFrame(title);
        frame.setSize(setSizeX, setSizeY);
        frame.setLocation(setLocX, setLocY);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        frame.setVisible(true);
        return frame;
    }
}
