package bank.dao;

import java.util.ArrayList;

public class Customer {
    private String PPS;
    private String surname;
    private String firstName;
    private String DOB;
    private String customerID;
    private String password;
    private ArrayList<CustomerAccount> accounts = new ArrayList<>();

    public Customer() {
    }

    public Customer(String PPS, String surname, String firstName, String DOB, String customerID, String password, ArrayList<CustomerAccount> accounts) {
        this.PPS = PPS;
        this.surname = surname;
        this.firstName = firstName;
        this.DOB = DOB;
        this.customerID = customerID;
        this.password = password;
        this.accounts = accounts;
    }

    public String getPPS() {
        return PPS;
    }

    public void setPPS(String PPS) {
        this.PPS = PPS;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<CustomerAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<CustomerAccount> accounts) {
        this.accounts = accounts;
    }

    public String toString() {
        return "PPS number = " + this.PPS + "\n"
                + "Surname = " + this.surname + "\n"
                + "First Name = " + this.firstName + "\n"
                + "Date of Birth = " + this.DOB + "\n"
                + "Customer ID = " + this.customerID;
    }
}
